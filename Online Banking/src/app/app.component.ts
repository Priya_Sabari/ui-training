import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'BankApp';

  DisplaySidebar:boolean=false;
  DisplayMainPanel=false;
  constructor(private router:Router) {
    
    
  }
  ngOnInit(): void {
    if(localStorage.getItem("accountno")!=null)
    {
      this.DisplaySidebar=true;
    
    }
  }
  Login()
  {
    this.DisplayMainPanel=false;
    this.DisplaySidebar=true;
    
  }

  Logout(){
    localStorage.clear();
    this.DisplayMainPanel=true;
    this.DisplaySidebar = false;
    this.router.navigate(['']);
    alert("Signed Out Succesfully...");
  }
}
