import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountBalanceComponent } from './Pages/account-balance/account-balance.component';
import { AccountSummaryComponent } from './Pages/account-summary/account-summary.component';
import { FundsTransferComponent } from './Pages/funds-transfer/funds-transfer.component';
import { TransactionDetailsComponent } from './Pages/transaction-details/transaction-details.component';
import { UserLoginComponent } from './Pages/user-login/user-login.component';
import { PageNotFoundComponent } from './Pages/page-not-found/page-not-found.component';
import { BenificiaryAccountComponent } from './Pages/benificiary-account/benificiary-account.component';


const routes: Routes = [
  {path:'account-balance',component:AccountBalanceComponent},
  {path:'account-summary',component:AccountSummaryComponent},
  {path:'fund-transfer',component:FundsTransferComponent},
  {path:'transaction-details',component:TransactionDetailsComponent},
  { path: 'user-login', component: UserLoginComponent },
   {path:'benificiary',component:BenificiaryAccountComponent } ,
  {path:'',component:UserLoginComponent},
  {path:'**',component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
