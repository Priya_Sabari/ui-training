import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountBalanceComponent } from './Pages/account-balance/account-balance.component';
import { AccountSummaryComponent } from './Pages/account-summary/account-summary.component';
import { FundsTransferComponent } from './Pages/funds-transfer/funds-transfer.component';
import { TransactionDetailsComponent } from './Pages/transaction-details/transaction-details.component';
import { UserLoginComponent } from './Pages/user-login/user-login.component';
import { PageNotFoundComponent } from './Pages/page-not-found/page-not-found.component';
import { UserService } from './ServiceLayer/user.service';
import { AccountService } from './ServiceLayer/account.service';
import { BenificiaryAccountComponent } from './Pages/benificiary-account/benificiary-account.component';
import { HomeComponent } from './Pages/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    AccountBalanceComponent,
    AccountSummaryComponent,
    FundsTransferComponent,
    TransactionDetailsComponent,
    UserLoginComponent,
    PageNotFoundComponent,
    BenificiaryAccountComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [UserService,AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
