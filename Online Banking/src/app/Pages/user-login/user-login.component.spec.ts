import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginComponent } from './user-login.component';

describe('UserLoginComponent', () => {
  let component: UserLoginComponent;
  let fixture: ComponentFixture<UserLoginComponent>;
  let h3:        HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not have welcome message after construction', () => {
    expect(component.welcome).toBeUndefined();
  });
  
  it('should display original title', () => {
    expect(h3.textContent).toContain(component.welcome);
  });

  it('no title in the DOM after createComponent()', () => {
    expect(h3.textContent).toEqual('');
  });

  it('should display original title after detectChanges()', () => {
    fixture.detectChanges();
    expect(h3.textContent).toContain(component.welcome);
  });

//test that changes the component's title property before calling fixture.detectChanges().
  it('should display a different test title', () => {
    component.welcome = 'Test Title';
    fixture.detectChanges();
    expect(h3.textContent).toContain('Test Title');
  });


});
