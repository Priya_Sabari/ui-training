import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/ServiceLayer/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  user_name:any;
  user_password:any;
  status_msg:any;
  status_color:any;
  isvaliduser:boolean=false;
  welcome: any;
  constructor(private userservice:UserService,private router:Router) { }

  ngOnInit(): void {
  }
//its used to validate the user data
  Login(userinfo)
  {
    debugger;
    this.userservice.isValiduser(userinfo.txt_user,userinfo.txt_password).subscribe(data=>{
      debugger;
      if(data.length==1)
      {

       this.userservice.setuserDetails(data[0].id,data[0].accountno,data[0].total_accountbalance,userinfo.txt_user,userinfo.txt_password);
      this.isvaliduser=true;
      this.status_color="green";
      this.status_msg="Login Successfully";
      this.router.navigate(['/account-balance']);

      }
      if(data.length==0){
        this.isvaliduser=false;
        this.status_color="red";
        this.status_msg="Login Failed";
      }
    })
    
  }

   

}
