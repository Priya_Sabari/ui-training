import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/ServiceLayer/user.service';
import { AccountService } from 'src/app/ServiceLayer/account.service';
import { BenificiaryService } from 'src/app/ServiceLayer/benificiary.service';

@Component({
  selector: 'app-funds-transfer',
  templateUrl: './funds-transfer.component.html',
  styleUrls: ['./funds-transfer.component.css']
})
export class FundsTransferComponent implements OnInit {

  from_account: any;
  to_account: any;
  amount: any;
  transfer_type: any;
 
  isTwoAccountsNumbers_Valid = false;
  my_balance: any;
    
  //here we are calling the services of user and account
 constructor(private userservice: UserService, private accountservice: AccountService,
              private benificiaryservice:BenificiaryService) { }

  ngOnInit(): void {
   
    this.my_balance = localStorage.getItem("totalbalnce");
    this.from_account = localStorage.getItem("accountno");
    this.to_account = this.benificiaryservice.getBenificiaryAccountnumber();
  }

//this method is used to send the amount to respective user account
  sendamount(accountinfo) {

    this.CheckLoginUserAccountnumber(accountinfo.txt_from_account);
    this.checkAccountExists(accountinfo.txt_to_account);
 
    //sending the data to the api fields by taking value from the screen
    let formdata = {
      "from_account_no": accountinfo.txt_from_account,
      "to_account_no": accountinfo.txt_to_account,
      "transfer_amount": accountinfo.txt_amount,
      "transfer_type": accountinfo.ddl_transfer_type
    }

    if (this.isTwoAccountsNumbers_Valid == true && accountinfo.txt_amount!=undefined&&accountinfo.ddl_transfer_type!=undefined)
    {
      this.accountservice.TransferAmount(formdata).subscribe(data => {
        alert("Funds Transfer Done Successfully");
  debugger;
        let account_seq_id=localStorage.getItem('id');
      //this method calls Update Amount after the amount is transferred
      let updateformdata={
          "accountno": localStorage.getItem("accountno"),
          "name": localStorage.getItem("name"),
          "password": localStorage.getItem("password"),
          "total_accountbalance": parseInt(this.my_balance) - parseInt(accountinfo.txt_amount)
        }
        this.accountservice.UpdateAccountBalance(account_seq_id,updateformdata).subscribe(data=>{
          
          this.my_balance=data.total_accountbalance;
          debugger;
          localStorage.setItem("totalbalnce",this.my_balance);
          alert("Total Balance Updated Successfully");
        })
      });
    }
  }
  
//here we check the validation of Our account number whether its right or not
  CheckLoginUserAccountnumber(accountnumber) {
    if (accountnumber != localStorage.getItem("accountno")) {
      this.isTwoAccountsNumbers_Valid = false;
      alert("From Account Number is not valid")
    }
  }
 
//this method checks TO user account valid or not
  checkAccountExists(accountnumber) {
    debugger;
    if (accountnumber != '') {
      this.userservice.getspecificuser(accountnumber).subscribe(data => {
        if (data.length == 0) {
          this.isTwoAccountsNumbers_Valid = false;
          alert("Invalid  To Account Number");
        }
        if (data.length == 1) {
          this.isTwoAccountsNumbers_Valid = true;
      
        }
      });
    }

  }
}
