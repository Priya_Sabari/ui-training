import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/ServiceLayer/user.service';

@Component({
  selector: 'app-account-summary',
  templateUrl: './account-summary.component.html',
  styleUrls: ['./account-summary.component.css']
})
export class AccountSummaryComponent implements OnInit {
  AccountDetails:any;
  constructor(private userservice:UserService) { }

  ngOnInit(): void {
    this.DisplayAccountDetails()
  }
//this method is used to get account details based on user who loggedin and display it
  DisplayAccountDetails()
  {
    let accountno=localStorage.getItem("accountno");
    this.userservice.getspecificuser(accountno).subscribe(data=>{
      this.AccountDetails=data[0];
    })
  }
}
