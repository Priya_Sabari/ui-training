import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/ServiceLayer/account.service';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.css']
})
export class TransactionDetailsComponent implements OnInit {

  all_transaction_info:any;
  constructor(private accountservice:AccountService) { }

  ngOnInit(): void {
    this.DisplayAllTransactionDetails();
  }
//this method is used to display all the transaction made for this account
  DisplayAllTransactionDetails()
  {
    this.accountservice.GetMyTransactionDetails(localStorage.getItem("accountno")).subscribe(data=>{
      this.all_transaction_info=data;
    })
  }
  //this method is used to delete the transaction made
  DeleteTransaction(account_seq_id)
  {
    this.accountservice.DeleteTransaction(account_seq_id).subscribe(data=>{
      alert( "Transaction Id : " + account_seq_id +" Deleted Successfully");
      this.DisplayAllTransactionDetails();
    })
  }
}
