import { Component, OnInit } from '@angular/core';
import { BenificiaryService } from 'src/app/ServiceLayer/benificiary.service';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-benificiary-account',
  templateUrl: './benificiary-account.component.html',
  styleUrls: ['./benificiary-account.component.css']
})
export class BenificiaryAccountComponent implements OnInit {
  benificiary_account_no: any;
  benificiary_account_name: any;
  benificiary_ifsc_code: any;

  my_benificiary_list: any;
  //calling the services
  constructor(private benificiaryservice:BenificiaryService,private router:Router) { }

  ngOnInit(): void {
    this.DisplayMyBenificiaryList();
  }
//this method is used to save the beneficiary details
  saveBenificiary(benificiaryinfo)
  {
      //sending the data to the api fields by taking value from the screen

    let formdata = {
      "main_account_no": localStorage.getItem("accountno"),
      "beneficiary_account": benificiaryinfo.txt_benificiary_account_no,
      "beneficiary_name": benificiaryinfo.txt_benificiary_account_name,
      "benificiary_ifsc": benificiaryinfo.txt_benificiary_ifsc_code
    } 

  if( benificiaryinfo.txt_benificiary_account_no==undefined||benificiaryinfo.txt_benificiary_account_name==undefined||benificiaryinfo.txt_benificiary_ifsc_code==undefined ){
  alert("Please fill all the beneficiary details");
return false;
    }else{
      this.benificiaryservice.AddBenificiary(formdata).subscribe(data => {
        alert("Beneficiary Details Added Successfully...");
        this.clearformdata();
        this.DisplayMyBenificiaryList();
      })
    }

  }
//this method is used to display the beneficiary list to users while making transaction
  DisplayMyBenificiaryList()
  {
    let my_account_no = localStorage.getItem("accountno");
    this.benificiaryservice.GetMyBenificiaryList(my_account_no).subscribe(data => {
      this.my_benificiary_list = data;
    })
  }

  //this method is used to delete the beneficiary added
  Deletebenificiary(benificiary_seq_id,benificiary_name)
  {
    if (confirm("Do you want to Delete Benificiary Name : " + benificiary_name + " ?")==true)
    {
      this.benificiaryservice.DeleteBenificiary(benificiary_seq_id).subscribe(data => {
        alert("Deleted Succesfully");
        this.DisplayMyBenificiaryList();
    })
       
    }
   
  }
//its used to goto fund transfer page for making the transcation
  gotofundTransferpage(benificiry_account_no)
  {
    this.benificiaryservice.setBenificiaryAccountnumber(benificiry_account_no);
    this.router.navigate(['/fund-transfer']);
  }

  clearformdata()
  {
    this.benificiary_account_no = '';
    this.benificiary_account_name = '';
    this.benificiary_ifsc_code = '';
  }

}
