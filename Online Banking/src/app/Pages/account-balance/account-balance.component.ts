import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/ServiceLayer/account.service';

@Component({
  selector: 'app-account-balance',
  templateUrl: './account-balance.component.html',
  styleUrls: ['./account-balance.component.css']
})
export class AccountBalanceComponent implements OnInit {

  my_total_balance:any;
  constructor(private accountservice:AccountService) { }

  //this method is used to fetch the total balance and display it
  ngOnInit() {  
    this.my_total_balance=localStorage.getItem("totalbalnce");
    this.accountservice.GetTotalAccountBalance().subscribe(data=>{

      this.my_total_balance=localStorage.getItem("totalbalnce");

 })
  } 

}
