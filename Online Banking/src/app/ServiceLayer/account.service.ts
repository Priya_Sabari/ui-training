import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  total_balance:any;
  Base_url=environment.Transaction_url;
  User_url=environment.Userinfo_url;
  constructor(private http:HttpClient,private userservice:UserService) { }

  GetTotalAccountBalance():Observable<any>
  {
    let username=localStorage.getItem("name");
    let password=localStorage.getItem("password");
    let url=this.Base_url +'?name='+username +'&password=' +password;
    return this.http.get(url);
  }
  GetMyTransactionDetails(my_account_number) :Observable<any>
  {
    let url=this.Base_url +'?from_account_no='+my_account_number;
    return this.http.get(url);
  }
  TransferAmount(acccountinfo):Observable<any>
  {
    let url=this.Base_url;
    return this.http.post(url,acccountinfo);
  }
  UpdateAccountBalance(account_seq_id,acccountinfo):Observable<any>
  {
    debugger;
    account_seq_id=localStorage.getItem("id");
    let url=this.User_url+'/' +account_seq_id;
    return this.http.put(url,acccountinfo);
  }
  DeleteTransaction(account_seq_no):Observable<any>
  {
    let url=this.Base_url + '/' + account_seq_no;
    return this.http.delete(url);

  }
}













