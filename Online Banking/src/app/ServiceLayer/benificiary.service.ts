import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BenificiaryService {

  Base_Url = environment.Benificiaryinfo_url;
  selected_benificiary_account_no: any;
  constructor(private http: HttpClient) { }
  
  AddBenificiary(formdata):Observable<any>
  {
    return this.http.post(this.Base_Url, formdata);
  }
  DeleteBenificiary(benificiary_seq_id):Observable<any>
  {
    let url = this.Base_Url + '/' + benificiary_seq_id;
    return this.http.delete(url);
  }
  GetMyBenificiaryList(my_account_no): Observable<any>
  {
    let url = this.Base_Url + '?main_account_no=' + my_account_no;
    return this.http.get(url);
  }

  setBenificiaryAccountnumber(accoun_no)
  {
    this.selected_benificiary_account_no = accoun_no;
  }
  getBenificiaryAccountnumber()
  {
    return this.selected_benificiary_account_no;
  }
}
