import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  Base_url = environment.Userinfo_url;

  valid_user_status: boolean;
  constructor(private http: HttpClient) { }

  isValiduser(user_name, user_password): Observable<any> {
    debugger;
    let url = this.Base_url + '?name=' + user_name + '&password=' + user_password;
    return this.http.get(url);
  }

  getAllValidusers(): Observable<any>
  {
    let url = this.Base_url;
    return this.http.get(url);
  }
  getspecificuser(accountnumber): Observable<any> 
  {
    let url = this.Base_url+ '?accountno='+accountnumber;
    return this.http.get(url);
  }
  getUserDetails() {
    localStorage.getItem("accountno");
    localStorage.getItem("id");
    localStorage.getItem("totalbalnce");
    localStorage.getItem("name");
    localStorage.getItem("password");
  }
  setuserDetails(id,accountnumber,total_balance,username, userpassword): void {
    localStorage.setItem("id", id);
    localStorage.setItem("totalbalnce", total_balance);
    localStorage.setItem("accountno", accountnumber);
    localStorage.setItem("name", username);
    localStorage.setItem("password", userpassword);
  }
 


}
