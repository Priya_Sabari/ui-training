//Its used to register user to shopping
function register()
{
  
   
    let username =  document.getElementById("username").value;
    let mobile = document.getElementById("mobile").value;
    let email = document.getElementById("email").value;
    let pwd = document.getElementById("password").value;
    let address  = document.getElementById("address").value;
    let pattern="[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";

    if(username=="" || username==null){
    alert("Please Enter username"); 
    return false;       
    }

    else if(mobile=="" || mobile==null){
    alert("Please Enter Mobile");
    return false;       
    }
    else if(mobile.length!=10){
    alert("Please Enter 10 digit Mobile Number");
    return false;       
    }
    else if(email=="" || email==null){
    alert("Please Enter Email");
    return false;       
    }

    else if(!email.match(pattern)){
    alert("You have to enter either DBS/HCL mail id to register");
    return false;       
    }

    else if(pwd=="" || pwd==null){
        alert("Please enter Password");
        return false;
    }

    else if(address=="" || address==null){
        alert("Please Enter your address");
        return false;       
    }
    else if(address.length<10){
        alert("Please enter address completely");
        return false;       
    }
     console.log("Validations completed");


    var httpReq;
    if(window.XMLHttpRequest)
    {
    httpReq = new XMLHttpRequest
    }
    else{
    httpReq  = new ActiveXObject();
    }
    httpReq.onreadystatechange = () =>
    {
            if(this.readyState===4 && this.status===201)
            {
                    alert("User added successfully");
                    window.location.assign("login.html");
                
            }
    } 

//global variable
let obj = {username:username,mobile:mobile,email:email,password:password,address:address};

   console.log(obj);
 

    httpReq.open('POST',"http://localhost:3000/users",true);
    httpReq.setRequestHeader("Content-type","application/json");
    httpReq.send(JSON.stringify(obj));


}


