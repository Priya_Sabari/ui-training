appointmentsBooked();
//It shows doctors booked appointments
function appointmentsBooked()
{
    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
  if(this.readyState===4 && this.status===200)
  {
 
    var appointmentHeading =  document.createElement("h2");
    appointmentHeading.appendChild(document.createTextNode("Appointments"));
    appointmentHeading.setAttribute("id",'headingID');

      var table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      var thead = document.createElement('thead');
      var tbody = document.createElement('tbody');

      var headTr = document.createElement('tr');

      var td1 = document.createElement('td');
      var td1Text = document.createTextNode("Doctor MailID");
      td1.appendChild(td1Text);

      var td2 = document.createElement('td');
      var td2Text = document.createTextNode("Date");
      td2.appendChild(td2Text);

      var td3 = document.createElement('td');
      var td3Text = document.createTextNode("Time");
      td3.appendChild(td3Text);

      var td4 = document.createElement('td');
      var td4Text = document.createTextNode("Type");
      td4.appendChild(td4Text);

      var td5 = document.createElement('td');
      var td5Text = document.createTextNode("Patient Email");
      td5.appendChild(td5Text);

     
      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
   

      thead.appendChild(headTr);
 

       var data = JSON.parse(this.response);
       var len = data.length;
      if(len>0)
      {
       for(var i=0;i<len;i++)
       {
          
           
           var tbodyTr = document.createElement("tr");
           var td1 = document.createElement("td");
           var td2 = document.createElement("td");
           var td3 = document.createElement("td");
           var td4 = document.createElement("td");
           var td5 = document.createElement("td");


           var td1TextNode = document.createTextNode(data[i].doctoremail);
           var td2TextNode = document.createTextNode(data[i].date);
           var td3TextNode = document.createTextNode(data[i].time);
           var td4TextNode = document.createTextNode(data[i].type);
           var td5TextNode = document.createTextNode(data[i].patientemail);
       
      

    

           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5TextNode);      

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);


           tbody.appendChild(tbodyTr);
         

       }
      }
      else{
            var data = document.createElement("h4");
            var noData = document.createTextNode("No Booking Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      var body = document.getElementsByTagName('body')[0];
      
      body.appendChild(appointmentHeading);
      body.appendChild(table);
    
  }
}
var doctoremail = sessionStorage.getItem('user');

httpReq.open('GET',"http://localhost:3000/appointment?doctoremail="+doctoremail,true);
httpReq.send();
}

function addSlot()
{
    window.location.assign("addslot.html");
}

function viewSlots()
{
    window.location.assign("viewslots.html");
}
