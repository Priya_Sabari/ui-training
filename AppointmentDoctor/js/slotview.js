getSlots();
//Used to get slots which are not booked
function getSlots()
{  
    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
        httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpReq.onreadystatechange = function()
    {
  if(this.readyState===4 && this.status===200)
  {
     


var appointmentHeading =  document.createElement("h2");
appointmentHeading.appendChild(document.createTextNode("Available slots to book"));
appointmentHeading.setAttribute("id",'headingID');


var table = document.createElement('table');
table.setAttribute("id",'dataTable');

var thead = document.createElement('thead');
var tbody = document.createElement('tbody');

var headTr = document.createElement('tr');

var td1 = document.createElement('td');
var td1Text = document.createTextNode("Date");
td1.appendChild(td1Text);

var td2 = document.createElement('td');
var td2Text = document.createTextNode("Time");
td2.appendChild(td2Text);

var td3 = document.createElement('td');
var td3Text = document.createTextNode("Type");
td3.appendChild(td3Text);

headTr.appendChild(td1);
headTr.appendChild(td2);
headTr.appendChild(td3);

thead.appendChild(headTr);

 
   var data = JSON.parse(this.response);
   var len = data.length;
   if(len>0)
  {
   for(var i=0;i<len;i++)
   {       
       var tbodyTr = document.createElement("tr");
       var td1 = document.createElement("td");
       var td2 = document.createElement("td");
       var td3 = document.createElement("td");   

       var td1TextNode = document.createTextNode(data[i].date);
       var td2TextNode = document.createTextNode(data[i].time);
       var td3TextNode = document.createTextNode(data[i].specialist);
    
       td1.appendChild(td1TextNode);
       td2.appendChild(td2TextNode);
       td3.appendChild(td3TextNode);
      
       tbodyTr.appendChild(td1);
       tbodyTr.appendChild(td2);
       tbodyTr.appendChild(td3);



       tbody.appendChild(tbodyTr);
     

   }
  }
  else{
        var data = document.createElement("h4");
        var noData = document.createTextNode("Data Not Available");
        data.appendChild(noData);
        tbody.appendChild(data);
  }
   
  table.appendChild(thead);
  table.appendChild(tbody);
  

  var body = document.getElementsByTagName('body')[0];
  
  body.appendChild(appointmentHeading);
  body.appendChild(table);
}
    } 

var email = sessionStorage.getItem('user');
var no = "no";

httpReq.open('GET',"http://localhost:3000/slots?doctoremail="+email+"&booked="+no,true);
httpReq.send();
}
