viewSlotsAvailable();
//It shows slots of all available doctors
function viewSlotsAvailable()
{
    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
  if(this.readyState===4 && this.status===200)
  {

    var appointmentHeading =  document.createElement("h2");
    appointmentHeading.appendChild(document.createTextNode("Available Slots"));
    appointmentHeading.setAttribute("id",'headingID');

      var table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      var thead = document.createElement('thead');
      var tbody = document.createElement('tbody');

      var headTr = document.createElement('tr');

      var td1 = document.createElement('td');
      var td1Text = document.createTextNode("Doctor MailId");
      td1.appendChild(td1Text);

      var td2 = document.createElement('td');
      var td2Text = document.createTextNode("Date");
      td2.appendChild(td2Text);

      var td3 = document.createElement('td');
      var td3Text = document.createTextNode("Time");
      td3.appendChild(td3Text);

      var td4 = document.createElement('td');
      var td4Text = document.createTextNode("Type");
      td4.appendChild(td4Text);

      var td5 = document.createElement('td');
      var td5Text = document.createTextNode("Illness Reason");
      td5.appendChild(td5Text);

      var td6 = document.createElement('td');
      var td6Text = document.createTextNode("Action");
      td6.appendChild(td6Text);

     
      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
      headTr.appendChild(td6);
   

      thead.appendChild(headTr);

       var data = JSON.parse(this.response);
       var len = data.length;
      if(len>0)
      {
       for(var i=0;i<len;i++)
       {            
           var tbodyTr = document.createElement("tr");
           var td1 = document.createElement("td");
           var td2 = document.createElement("td");
           var td3 = document.createElement("td");
           var td4 = document.createElement("td");
           var td5 = document.createElement("td");
           var td6 = document.createElement("td");

           var slotId = data[i].id;
           var td1TextNode = document.createTextNode(data[i].doctoremail);
           var td2TextNode = document.createTextNode(data[i].date);
           var td3TextNode = document.createTextNode(data[i].time);
           var td4TextNode = document.createTextNode(data[i].type);
           var td5InputElement = document.createElement("input");
           td5InputElement.setAttribute("type","text");
           td5InputElement.setAttribute("id","res");

       var bookAppointmentButton = document.createElement("button");
       var bookAppointmentButtonTextNode = document.createTextNode("Book Appointment");
       bookAppointmentButton.appendChild(bookAppointmentButtonTextNode);
       bookAppointmentButton.addEventListener('click',function()
       {
           var reason = document.getElementById("res").value;
           if(reason.trim().length<1||reason==="" || reason===null){
                alert("Enter illness");
            return false;       
            }
            var httpReq1;
            if(window.XMLHttpRequest)
            {
                httpReq1 = new XMLHttpRequest
            }
            else{
                httpReq1  = new ActiveXObject("Microsoft.XMLHTTP");
            }

           var data = this.parentElement.parentElement.cells;
           console.log(data);
           console.log(slotId);
           var reason = data[4].childNodes[0].value;
        
           var obj2 = {doctoremail:data[0].innerHTML,date:data[1].innerHTML,
            time:data[2].innerHTML,specialist:data[3].innerHTML,
            reason:reason,
            patientemail:sessionStorage.getItem('user')};
            httpReq1.onreadystatechange = function()
            {
                if(this.readyState===4 && this.status===201)
                {
                    var httpReq2;
                    if(window.XMLHttpRequest)
                    {
                        httpReq2 = new XMLHttpRequest
                    }
                    else{
                        httpReq2  = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    

                    var obj3 =  {doctoremail:data[0].innerHTML,
                        
                    date:data[1].innerHTML,time:data[2].innerHTML,
                    type:data[3].innerHTML,
                    booked:"yes"
                };
                    console.log(slotId);
                    console.log(obj3);
                    httpReq2.onreadystatechange = function()
                    {
                        if(this.readyState===4 && this.status===200)
                        {
                            window.alert("Appointment booked Successfully");
                        }
                    }
                    httpReq2.open('PUT',"http://localhost:3000/slots/"+slotId,false);
                    httpReq2.setRequestHeader("Content-type","application/json");
                    httpReq2.send(JSON.stringify(obj3));
                    
                }
            }

            httpReq1.open('POST',"http://localhost:3000/appointment",false);
            httpReq1.setRequestHeader("Content-type","application/json");
            httpReq1.send(JSON.stringify(obj2));
           
       })
      
           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5InputElement);
           td6.appendChild(bookAppointmentButton);       

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);
           tbodyTr.appendChild(td6);

           tbody.appendChild(tbodyTr);        

       }
      }
      else{
            var data = document.createElement("h4");
            var noData = document.createTextNode("No Data Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      var body = document.getElementsByTagName('body')[0];
      
      body.appendChild(appointmentHeading);
      body.appendChild(table);
  }
}

httpReq.open('GET',"http://localhost:3000/slots?booked=no",true);
httpReq.send();
}

