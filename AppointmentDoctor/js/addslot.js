//add slot for doctors
function addSlot()
{
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var type = document.getElementById("type").value;

    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
            if(this.readyState===4 && this.status===201)
            {
                    alert("Successfully Slot is added");
                
            }
    } 

    var doc = {
        doctoremail:sessionStorage.getItem('user'),
        date:date,
        time:time,
        type:type,
        booked:"no"
    };

    console.log(doc);
 
     httpReq.open('POST',"http://localhost:3000/slots",true);
     httpReq.setRequestHeader("Content-type","application/json");
     httpReq.send(JSON.stringify(doc));
 


}
//It checks if doctor already exists
function checkAvailability()
{
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var type = document.getElementById("type").value;
    var email = sessionStorage.getItem('user');

    //data validation
    var curDate = new Date();
    if(date===null||!date.length>0){
         alert("Please select date for booking doctor's appointment");
    return false;
    }
    
    if(time===null||!time.length>0){
    alert("Please select time for booking doctor's appointment");
    return false;
    }
   
		


    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
            if(this.readyState===4 && this.status===200)
            {
                var data = JSON.parse(this.response);
                var len = data.length;
                if(len>0)
                {
                    alert("For this selected day Slot exists");
                    return false;
                }
                else
                   addSlot();
            }
    } 

    
    httpReq.open('GET',"http://localhost:3000/slots?date="+date+"&time="+time+"&type="+type+"&doctoremail="+email,true);
    httpReq.send();



}
